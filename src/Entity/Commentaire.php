<?php

namespace App\Entity;

use App\Repository\CommentaireRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"show", "list"})
     * @Assert\NotBlank(groups={"Create"})
     */
    private $texte;

    /**
     * @ORM\OneToOne(targetEntity=Note::class, mappedBy="commentaire")
     * @Groups({"show", "list"})
     * @Assert\NotBlank(groups={"Create"})
     */
    private $note;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getNote(): ?Note
    {
        return $this->note;
    }

    public function setNote(?Note $note): self
    {
        $this->note = $note;

        return $this;
    }
}
