<?php

namespace App\Entity;

use App\Entity\Personne;
use App\Repository\EleveRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EleveRepository::class)
 */
class Eleve extends Personne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show", "list", "post"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    protected $prenom;

    /**
     * @ORM\Column(type="date")
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    protected $dateDeNaissance;

    /**
     * @ORM\ManyToOne(targetEntity=Classe::class, inversedBy="eleves")
     * @Groups({"show", "list"})
     */
    private $classe;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="eleve")
     * @Groups({"show", "list"})
     */
    private $notes;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->dateDeNaissance = new \DateTime();
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(?Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setEleve($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getEleve() === $this) {
                $note->setEleve(null);
            }
        }

        return $this;
    }
}
