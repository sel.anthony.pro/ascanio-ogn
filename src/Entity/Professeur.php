<?php

namespace App\Entity;

use App\Entity\Personne;
use App\Repository\ProfesseurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProfesseurRepository::class)
 */
class Professeur extends Personne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show", "list", "post"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    protected $prenom;

    /**
     * @ORM\Column(type="date")
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    protected $dateDeNaissance;

    /**
     * @ORM\OneToMany(targetEntity=CoursSuivi::class, mappedBy="professeur")
     */
    private $coursSuivis;

    public function __construct()
    {
        $this->coursSuivis = new ArrayCollection();
        $this->dateDeNaissance = new \DateTime();
    }

    /**
     * @return Collection|CoursSuivi[]
     */
    public function getCoursSuivis(): Collection
    {
        return $this->coursSuivis;
    }

    public function addCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if (!$this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis[] = $coursSuivi;
            $coursSuivi->setProfesseur($this);
        }

        return $this;
    }

    public function removeCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if ($this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis->removeElement($coursSuivi);
            // set the owning side to null (unless already changed)
            if ($coursSuivi->getProfesseur() === $this) {
                $coursSuivi->setProfesseur(null);
            }
        }

        return $this;
    }
}
