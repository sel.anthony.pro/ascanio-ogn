<?php

namespace App\Controller\API;

use App\Entity\Classe;
use App\Repository\ClasseRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use App\Exception\ResourceValidationException;

/**
 * @Rest\Route("api")
 */
class ClasseController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var ClasseRepository
     */
    private $classeRepository;

    public function __construct(EntityManagerInterface $em, ClasseRepository $classeRepository)
    {
        $this->em = $em;
        $this->classeRepository = $classeRepository;
    }
    /**
     * @Rest\Get(
     *     path="/classes/{id}",
     *     name="api_classes_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"show"})
     */
    public function show(Classe $classe)
    {
        if ($classe) {
            return $classe;
        }
    }

    /**
     * @Rest\Get(
     *     path="/classes",
     *     name="api_classes_liste",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function list()
    {
        return $this->classeRepository->findAll();
    }

    /**
     * @Rest\Post(
     *     path="/classes",
     *     name="api_classes_create"
     * )
     * @Rest\View(StatusCode=201, serializerGroups={"post"})
     * @ParamConverter(
     *      "classe",
     *      converter="fos_rest.request_body",
     *      options={
     *          "validator"={"groups"="Create"}
     *      })
     *
     */
    public function post(Classe $classe, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            $message = 'Le JSON contient des données invalides: ';
            foreach ($violations as $violation) {
                $message .= sprintf("Field %s: %s ", $violation->getPropertyPath(), $violation->getMessage());
            }

            throw new ResourceValidationException($message);
        }

        $this->em->persist($classe);
        $this->em->flush();

        return $this->view($classe, Response::HTTP_CREATED, ['Location' => $this->generateUrl('api_classes_detail', ['id' => $classe->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
    }

}
