<?php

namespace App\Controller\API;

use App\Entity\Eleve;
use App\Entity\Classe;
use App\Repository\EleveRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use App\Exception\ResourceValidationException;

/**
 * @Rest\Route("api")
 */
class EleveController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var EleveRepository
     */
    private $eleveRepository;

    public function __construct(EntityManagerInterface $em, EleveRepository $eleveRepository)
    {
        $this->em = $em;
        $this->eleveRepository = $eleveRepository;
    }
    /**
     * @Rest\Get(
     *     path="/eleves/{id}",
     *     name="api_eleves_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(serializerGroups={"show"})
     */
    public function show(Eleve $eleve)
    {
        if ($eleve) {
            return $eleve;
        }
    }

    /**
     * @Rest\Get(
     *     path="/eleves",
     *     name="api_eleves_liste",
     * )
     * @Rest\View(serializerGroups={"list"})
     */
    public function list()
    {
        return $this->eleveRepository->findAll();
    }

    /**
     * @Rest\Get(
     *     path="/search/eleves",
     *     name="api_eleves_searchList"
     * )
     * @Rest\QueryParam(
     *     name="nom",
     *     requirements="[a-zA-Z0-9]*",
     *     nullable=true,
     *     description="Le nom d'élève à trouver",
     * )
     * @Rest\QueryParam(
     *     name="classe",
     *     requirements="\d+",
     *     nullable=true,
     *     description="la classe d'élève à trouver",
     * )
     * @Rest\QueryParam(
     *     name="order",
     *     requirements="asc|desc",
     *     default="asc",
     *     description="Ordonnancement (asc ou desc)",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function search(ParamFetcherInterface $paramFetcher)
    {
        $eleves = $this->eleveRepository->search(
            $paramFetcher->get('nom'),
            $paramFetcher->get('classe'),
            $paramFetcher->get('order')
        );

        return $eleves;
    }

    /**
     * @Rest\Post(
     *     path="/classes/{classe_id}/eleves",
     *     name="api_eleves_create"
     * )
     * @Rest\View(StatusCode=201, serializerGroups={"post"})
     * @ParamConverter("eleve", converter="fos_rest.request_body", options={"validator":{"groups"="Create"}})
     * @ParamConverter("classe", options={"mapping": {"classe_id": "id"}})
     */
    public function post(Classe $classe, Eleve $eleve, ConstraintViolationList $violations)
    {
        $eleve->setClasse($classe);

        if (count($violations)) {
            $message = 'Le JSON contient des données invalides: ';
            foreach ($violations as $violation) {
                $message .= sprintf("Field %s: %s ", $violation->getPropertyPath(), $violation->getMessage());
            }

            throw new ResourceValidationException($message);
        }

        $this->em->persist($eleve);
        $this->em->flush();

        return $this->view($eleve, Response::HTTP_CREATED, ['Location' => $this->generateUrl('api_eleves_detail', ['id' => $eleve->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
    }

}
