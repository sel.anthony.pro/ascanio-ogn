<?php

namespace App\Controller\API;

use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Rest\Route("api")
 */
class CommentaireController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var CommentaireRepository
     */
    private $commentaireRepository;

    public function __construct(EntityManagerInterface $em, CommentaireRepository $commentaireRepository)
    {
        $this->em = $em;
        $this->commentaireRepository = $commentaireRepository;
    }
    /**
     * @Rest\Get(
     *     path="/commentaires/{id}",
     *     name="api_commentaires_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(serializerGroups={"show"})
     */
    public function show(Commentaire $commentaire)
    {
        if ($commentaire) {
            return $commentaire;
        }
    }

    /**
     * @Rest\Get(
     *     path="/commentaires",
     *     name="api_commentaires_liste",
     * )
     * @Rest\View(serializerGroups={"list"})
     */
    public function list()
    {
        return $this->commentaireRepository->findAll();
    }

    // TODO : Faire la mise à jour PUT
    /*
    public function put()
    {

    }
    */
}
