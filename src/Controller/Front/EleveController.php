<?php

namespace App\Controller\Front;

use App\Entity\Eleve;
use App\Entity\Matiere;
use App\Form\EleveType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EleveController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/eleves", name="eleve_list")
     */
    public function list()
    {
        // Récupération des élèves
        $responseEleve = $this->client->request(
            'GET',
            'http://localhost:8001/api/eleves'
        );

        // Récupération des classes
        $responseClasse = $this->client->request(
            'GET',
            'http://localhost:8001/api/classes'
        );

        $statusCodeEleve = $responseEleve->getStatusCode();
        $statusCodeClasse = $responseClasse->getStatusCode();

        if ($statusCodeEleve == "200" && $statusCodeClasse) {
            return $this->render('eleve/list.html.twig', [
                'eleves' => $responseEleve->toArray(),
                'classes' => $responseClasse->toArray()
            ]);
        } else {
            // TODO Retourner une erreur 404
        }
    }

    /**
     * @Route("/eleves/{id}", name="eleve_show")
     */
    public function show(int $id)
    {
        $response = $this->client->request(
            'GET',
            'http://localhost:8001/api/eleves/'.$id
        );

        $statusCode = $response->getStatusCode();

        if ($statusCode == "200") {
            $jsonData = $response->toArray();
            //dump($jsonData); die;
            return $this->render('eleve/show.html.twig', [
                'eleve' => $jsonData
            ]);
        }
    }

    /**
     * @Route("/search/eleves", name="eleve_search")
     */
    public function search(Request $request)
    {
        $response = $this->client->request(
            'GET',
            'http://localhost:8001/api/search/eleves', [
                'query' => [
                    'nom' => $request->query->get('nom'),
                    'classe' => $request->query->get('classe')
                ]
            ]
        );

        $statusCode = $response->getStatusCode();

        if ($statusCode == "200") {
            $jsonData = $response->toArray();

            return $this->render('eleve/eleve-item-list.html.twig', [
                'eleves' => $jsonData
            ]);
        }
    }

    /**
     * @Route("/eleves/{id}/notes", name="eleve_add_note")
     */
    public function editNote(int $id) {
        // TODO : Récupération des matières pour la création du formulaire
        /*
        $response = $this->client->request(
            'GET',
            'http://localhost:8001/api/eleves/'. $id . '/cours'
        );

        $form = $this->createForm(EleveType::class, $eleve);

        $response = new JsonResponse(
            array(
                'message' => 'success',
                'output' => $this->renderView('eleve/eleve-form-notes.html.twig', [
                    'entity' => $eleve,
                    'form' => $form->createView()
                ]),
            )
        );

        return $response;
        */
    }
}
