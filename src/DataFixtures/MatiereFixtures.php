<?php

namespace App\DataFixtures;

use App\Entity\Matiere;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MatiereFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Création des matières
        $aNomMatiere = array('Français', 'Latin', 'Maths', 'Histoire Géographie', 'Physique Chimie');
        foreach ($aNomMatiere as $nomMatiere) {
            $matiere = new Matiere();
            $matiere->setNom($nomMatiere);
            $manager->persist($matiere);
        }

        $manager->flush();
    }
}
