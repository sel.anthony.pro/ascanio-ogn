<?php

namespace App\DataFixtures;

use App\Entity\Classe;
use App\Entity\Eleve;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EleveFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Création des classes
        $index = 1;
        $aPrefixClasse = array('2nd', '1ere', 'Term');
        $aSuffixClasse = array('A', 'B');
        foreach ($aPrefixClasse as $pre) {
            foreach ($aSuffixClasse as $suf) {
                $classe = new Classe();
                $classe->setNom($pre.$suf);
                // Création des élèves
                for ($i=1; $i <= 20; $i++) {
                    $eleve = new Eleve();
                    $eleve->setNom('Eleve_Nom_'.$index);
                    $eleve->setPrenom('Eleve_Prenom_'.$index);
                    $classe->addEleve($eleve);
                    $index++;
                }
                $manager->persist($classe);
            }
        }

        $manager->flush();
    }
}
